from __future__ import print_function
import AST


def addToClass(cls):
    def decorator(func):
        setattr(cls, func.__name__, func)
        return func

    return decorator


@addToClass(AST.Node)
def printTree(self, indent=0):
    raise Exception("printTree not defined in class " + self.__class__.__name__)


@addToClass(AST.IntNum)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    print(bars + str(self.value))


@addToClass(AST.Error)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    # fill in the body


@addToClass(AST.Variable)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    print(bars + self.name)
    # self.value.printTree(indent + 1)


@addToClass(AST.BinExpr)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    print(bars + self.op)
    self.left.printTree(indent + 1)
    self.right.printTree(indent + 1)

@addToClass(AST.UnExpr)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    print(bars + self.op)
    self.expr.printTree(indent + 1)



@addToClass(AST.Break)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    print(bars + "BREAK")


@addToClass(AST.Continue)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    print(bars + "CONTINUE")


@addToClass(AST.FloatNum)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    print(bars + str(self.value))


@addToClass(AST.ForLine)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    print(bars + "FOR")
    self.variable.printTree(indent + 1)
    print(bars+"|RANGE")
    self.start_index.printTree(indent+2)
    self.end_index.printTree(indent+2)
    self.line.printTree(indent + 1)
#     TODO

@addToClass(AST.WhileLine)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    print(bars + "WHILE")
    self.condition.printTree(indent + 1)
    self.line.printTree(indent + 1)


@addToClass(AST.IfLine)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    print(bars + "IF")
    self.condition.printTree(indent + 1)
    print(bars + "THEN")
    self.true_line.printTree(indent + 1)
    if self.false_line is not None:
        print(bars + "ELSE")
        self.false_line.printTree(indent + 1)



@addToClass(AST.Line)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    self.line.printTree(indent)




@addToClass(AST.Lines)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    for line in self.lines:
        line.printTree(indent)


@addToClass(AST.MatrixInit)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    print(bars + "MATRIX")
    for line in self.matrix_lines:
        print(bars + "|VECTOR")
        line.printTree(indent + 2)


@addToClass(AST.MatrixLine)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    for expr in self.expressions:
        expr.printTree(indent)


@addToClass(AST.MatrixLines)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])

    for line in self.matrix_lines:
        line.printTree(indent + 1)


@addToClass(AST.Return)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    print(bars + "RETURN")


@addToClass(AST.PrintLine)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    print(bars + "PRINT")
    for value in self.values:
        if isinstance(value,AST.Node):
            value.printTree(indent+1)
        else:
            print(bars+'|'+value)
#     TODO string value

@addToClass(AST.Zeros)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    print(bars+'zeros')
    self.init_expr.printTree(indent+1)

@addToClass(AST.Ones)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    print(bars + 'zeros')
    self.init_expr.printTree(indent + 1)

@addToClass(AST.Eye)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    print(bars+'zeros')
    self.init_expr.printTree(indent+1)

@addToClass(AST.Return)
def printTree(self, indent=0):
    bars = "".join(["|" for _ in range(0, indent)])
    print(bars+'return')
    if self.value is not None:
        self.value.printTree()

# define printTree for other classes
# ...
