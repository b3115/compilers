#!/usr/bin/python

class Symbol:
    pass

class VariableSymbol(Symbol):

    def __init__(self, name):
        self.name=name
        self.type="Variable"
    #


class SymbolTable(object):

    def __init__(self, parent, name): # parent scope and symbol table name
        self.parent=parent
        self.name=name
        self.symbols={}
        self.type=""

    def put(self, name, symbol): # put variable symbol or fundef under <name> entry
        self.symbols[name]=symbol

    def get(self, name): # get variable symbol or fundef from <name> entry
        if name in self.symbols.keys():
            return self.symbols[name]
        elif self.parent is not None:
            return self.parent.get(name)
        else:
            return None

    def getParentScope(self):
        return self.parent

    def isInLoop(self):
        actual_scope=self
        while actual_scope.type!="for" and actual_scope.type!="while" and actual_scope.parent is not None:
            actual_scope=actual_scope.parent
        if actual_scope.type=="for" or actual_scope.type=="while":
            return True
        return False

    def isInFun(self):
        actual_scope = self
        while actual_scope.type != "fun" and actual_scope.parent is not None:
            actual_scope = actual_scope.parent
        if actual_scope.type == "fun":
            return True
        return False

    def pushScope(self, name, type):
        new_scope = SymbolTable(self, name)
        new_scope.type=type
        return new_scope

    def popScope(self):
        return self.parent


