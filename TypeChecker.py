#!/usr/bin/python
import AST
from SymbolTable import SymbolTable, Symbol, VariableSymbol


class NodeVisitor(object):

    def visit(self, node):
        method = 'visit_' + node.__class__.__name__
        visitor = getattr(self, method, self.generic_visit)
        return visitor(node)


    def generic_visit(self, node):        # Called if no explicit visitor function exists for a node.
        if isinstance(node, list):
            for elem in node:
                self.visit(elem)
        else:
            for child in node.children:
                if isinstance(child, list):
                    for item in child:
                        if isinstance(item, AST.Node):
                            self.visit(item)
                elif isinstance(child, AST.Node):
                    self.visit(child)

    # simpler version of generic_visit, not so general
    #def generic_visit(self, node):
    #    for child in node.children:
    #        self.visit(child)



class TypeChecker(NodeVisitor):
    def __init__(self):
        self.symbol_table=SymbolTable(None,'main')

    def visit_BinExpr(self, node):
        self.visit(node.left)
        self.visit(node.right)
        if node.op=='=' or (len(node.op)==2 and node.op[1]=='='): #TODO What if op='=='
            if not isinstance(node.left, AST.Variable):
                self.err(node,"left side of assignment must be variable")
            else:
                if self.symbol_table.get(node.left.name)==None:
                    self.symbol_table.put(node.left.name,VariableSymbol(node.left.name))
                    self.symbol_table.get(node.left.name).size=node.right.size
                    node.left.size=node.right.size

                if node.left.indexes is None and node.right.indexes is None:
                    if node.left.size!=node.right.size:
                        print('tu',node.left.size,node.right.size)
                        self.err(node,"left side of assignment must have the same shape as the right side")
                elif (node.left.indexes is not None and node.right.size!=(1,1))or(node.right.indexes is not None and node.left.size!=(1,1)):
                    self.err(node,"left side of assignment must have the same shape as the right side")
                else:
                    node.size=node.left.size
                    return
        else:
            if isinstance(node.left, AST.Variable) and self.symbol_table.get(node.left.name)is None: #not an assignment
                self.err(node, str.format("{} is not initialized",node.left.name))
            if isinstance(node.right, AST.Variable) and self.symbol_table.get(node.right.name)is None: #not an assignment
                self.err(node, str.format("{} is not initialized",node.right.name))

        if node.left.scalar and node.right.scalar:
            node.scalar = True
            node.value = eval(str(node.left.value) + node.op + str(node.right.value))
        elif node.op[0] == '.':
            if node.left.size == node.right.size:
                node.size = node.left.size
            else:
                self.err(node, str.format("left and right expressions around '{}' must have the same shape", node.op))
        elif node.op == '*':

            if node.left.size[1] == node.right.size[0]:
                node.size = (node.left.size[0], node.right.size[1])
            elif node.left.size == (1, 1): #scalar and matrix multiplication
                node.size = node.right.size
            elif node.right.size == (1, 1):#matrix and scalar multiplication
                node.size = node.left.size
            else:
                self.err(node,"left and right matrixes around '*' must have compatible size (left cols and right rows)")

        else:
            if node.left.size == node.right.size:
                node.size = node.left.size
            else:
                self.err(node,str.format("left and right expressions around '{}' must have the same shape",node.op))

    def visit_UnExpr(self,node):
        self.visit(node.expr)
        if (node.expr.scalar):
            node.scalar = True
            if node.op == '-':
                node.value = -node.expr.value
        if node.op == "'":
            rows, cols = node.expr.size
            node.size = (cols, rows)  # transpose
        else:
            node.size = node.expr.size

    def visit_MatrixInit(self, node):
        line_len = len(node.matrix_lines[0].expressions)
        for line in node.matrix_lines:
            if len(line.expressions) != line_len:
                self.err(node," Matrix init lines have different lengths")


    def visit_Lines(self, node):
        for line in node.lines:
            self.visit(line)

    def visit_Variable(self, node):
        variable = self.symbol_table.get(node.name)
        if variable is not None:
            node.size=variable.size
        if variable is None and node.indexes is not None:
            self.err(node, "Variable not initialized")
            return
        elif variable is not None and node.indexes is not None and node.indexes>variable.size:
            self.err(node,"Index out of range or incompatible types")
            return



    def visit_one_scalar_arg_built_in(self,node):
        self.visit(node.init_expr)
        if node.init_expr.scalar and node.init_expr.value>0:
            node.size = (node.init_expr.value, node.init_expr.value)
        else:
            self.err(node, "init value must be calculable at compilation time and it must be positive integer value")
    def visit_Zeros(self,node):
        self.visit_one_scalar_arg_built_in(node)

    def visit_Ones(self,node):
        self.visit_one_scalar_arg_built_in(node)

    def visit_Eye(self,node):
        self.visit_one_scalar_arg_built_in(node)

    def visit_WhileLine(self,node):
        self.symbol_table = self.symbol_table.pushScope("while","while")
        self.visit(node.condition)
        self.visit(node.line)
        self.symbol_table = self.symbol_table.popScope()

    def visit_ForLine(self,node):
        self.symbol_table = self.symbol_table.pushScope("for", "for")
        self.visit(node.variable)
        self.visit(node.start_index)
        self.visit(node.end_index)
        self.visit(node.line)
        self.symbol_table = self.symbol_table.popScope()

    def visit_IfLine(self,node):
        self.symbol_table = self.symbol_table.pushScope("if", "if")
        self.visit(node.condition)
        self.visit(node.true_line)
        if node.false_line is not None:
            self.visit(node.false_line)
        self.symbol_table = self.symbol_table.popScope()

    def visit_Break(self,node):
        if not self.symbol_table.isInLoop():
            self.err(node,"Break statement outside the loop")

    def visit_Continue(self,node):
        if not self.symbol_table.isInLoop():
            self.err(node,"Continiue statement outside the loop")

    def visit_Return(self,node):
        if not self.symbol_table.isInFun():
            self.err(node,"return statement outside any function")


    def err(self, node, message=""):
        print("Error in line:",node.lineno,message)

        


