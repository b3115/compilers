class Node(object):
    count = 0

    def __init__(self, lineno=0, children=None, scalar=False):
        # print(self,lineno)
        self.ID = str(Node.count)
        Node.count += 1
        self.lineno = lineno
        if children is not None:
            self.children = children
        else:
            self.children=[]
        self.scalar = scalar
        self.size=(0,0)
        self.indexes=None

    def accept(self, visitor):
        # print("visiting",self.__class__)
        return visitor.visit(self)

class IntNum(Node):
    def __init__(self, value, lineno=0):
        super().__init__(scalar=True, lineno=lineno)
        self.value = value
        self.size = (1, 1)

class FloatNum(Node):
    def __init__(self, value, lineno=0):
        super().__init__(scalar=True,lineno=lineno)
        self.value = value
        self.size = (1, 1)


class Variable(Node):
    def __init__(self, name, indexes=None, lineno=0):
        super().__init__(scalar=False,lineno=lineno)
        self.name = name
        if indexes is None:
            self.indexes=None
        else:
            self.indexes = tuple([index.value for index in indexes])



class UnExpr(Node):
    def __init__(self, op, expr, lineno=0):
        super().__init__(children=expr,lineno=lineno)
        self.op = op
        self.expr = expr



class BinExpr(Node):
    def __init__(self, op, left, right, lineno=0):
        super().__init__(lineno=lineno, children=[left,right])
        self.op = op
        self.left = left
        self.right = right




class Line(Node):
    def __init__(self, line, lineno=0):
        super().__init__(lineno=lineno, children=[line])
        self.line = line


class Lines(Node):
    def __init__(self, line, lines=None, lineno=0):
        super().__init__(lineno=lineno)
        if lines is None:
            self.lines = [line]
        else:
            self.lines = lines.lines
            self.lines.append(line)
        self.children=self.lines


class IfLine(Node):
    def __init__(self, condition, true_line, false_line=None, lineno=0):
        super().__init__(lineno=lineno, children=[condition, true_line])
        self.condition = condition
        self.true_line = true_line
        self.false_line = false_line
        if false_line is not None:
            self.children.append(false_line)



class WhileLine(Node):
    def __init__(self, condition, line, lineno=0):
        super().__init__(lineno=lineno, children=[condition,line])
        self.condition = condition
        self.line = line


class ForLine(Node):
    def __init__(self, variable, start_index, end_index, line, lineno=0):
        super().__init__(lineno=lineno, children=[variable,start_index,end_index,line])
        self.variable = variable
        self.start_index = start_index
        self.end_index = end_index
        self.line = line


class Break(Node):
    def __init__(self, lineno=0):
        super().__init__(lineno=lineno)


class Continue(Node):
    def __init__(self, lineno=0):
        super().__init__(lineno=lineno)


class Return(Node):
    def __init__(self, lineno=0, value=None):
        super().__init__(lineno=lineno)
        self.value=value


class PrintLine(Node):
    def __init__(self, value, values=None, lineno=0):
        super().__init__(lineno=lineno, children=[value])
        if values is None:
            self.values = [value]
        else:
            self.values = values
            self.values.append(value)


class MatrixLine(Node):
    def __init__(self, expr, expressions=None, lineno=0):
        super().__init__(lineno=lineno)
        if expressions is None:
            self.expressions = [expr]
        else:
            self.expressions = expressions
            self.expressions.append(expr)
        self.children=[self.expressions]


class MatrixLines(Node):
    def __init__(self, matrix_line, matrix_lines=None, lineno=0):
        super().__init__(lineno=lineno)
        if matrix_lines is None:
            self.matrix_lines = [matrix_line]
        else:
            self.matrix_lines = matrix_lines
            self.matrix_lines.append(matrix_line)
        self.children=self.matrix_lines


class MatrixInit(Node):
    def __init__(self, matrix_lines, lineno=0):
        super().__init__(lineno=lineno, children=[matrix_lines])
        self.matrix_lines = matrix_lines.matrix_lines



class Error(Node):
    def __init__(self, lineno=0):
        super().__init__(lineno=lineno)


class Eye(Node):
    def __init__(self, init_expr, lineno=0):
        super().__init__(lineno=lineno, children=[init_expr])
        self.init_expr = init_expr




class Ones(Node):
    def __init__(self, init_expr, lineno=0):
        super().__init__(lineno=lineno, children=[init_expr])
        self.init_expr = init_expr


class Zeros(Node):
    def __init__(self, init_expr, lineno=0):
        super().__init__(lineno=lineno, children=[init_expr])
        self.init_expr = init_expr


