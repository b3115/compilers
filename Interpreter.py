
import AST
import SymbolTable
from Memory import *
from visit import *
import numpy as np
import sys

sys.setrecursionlimit(10000)

class BreakException(Exception):
    pass

class ContinueException(Exception):
    pass

class ReturnException(Exception):
    def __init__(self,value):
        self.value=value

class Interpreter(object):

    def __init__(self):
        self.memory = MemoryStack()
        self.memory.push(Memory("global_memory"))

    @on('node')
    def visit(self, node):
        print('node on')

    @when(AST.IntNum)
    def visit(self, node):
        return [[node.value]]

    @when(AST.FloatNum)
    def visit(self, node):
        return [[node.value]]

    @when(AST.Variable)
    def visit(self, node):
        if self.memory.lookup(node.name):
            return self.memory.lookup(node.name)[0]
        else:
            self.memory.register(node.name,0)



    @when(AST.UnExpr)
    def visit(self, node):
        value = node.expr.accept(self)
        print(value)
        if(node.op == '-'):
            return np.multiply(value,[[-1]])
        if (node.op == "'"):
            return np.transpose(value)




    @when(AST.BinExpr)
    def visit(self, node):
        op_map = {'.*':np.multiply,
                  '.+':np.add,
                  ".-":np.subtract,
                  './':np.divide,
                  '+':np.add,
                  "-":np.subtract,
                  '/':np.divide,
                  '==':np.array_equal,
                  "<":np.less,
                  "<=":np.less_equal}
        op_reversed_map={">":np.less_equal,">=":np.less,"!=":np.equal}
        assign_op_map = {'+=':np.add,"-=":np.subtract,'/=':np.divide}
        if node.op in op_map:
            return op_map[node.op](node.left.accept(self),node.right.accept(self))
        if node.op in op_reversed_map:
            return not op_reversed_map[node.op](node.left.accept(self),node.right.accept(self))
        if node.op in assign_op_map:
            value = assign_op_map[node.op](node.left.accept(self),node.right.accept(self))
            self.memory.register(node.left.name,value)
            return value
        if node.op[0] == '*':
            if node.left.shape==(1,1) or node.right.shape==(1,1):
                value = np.multiply(node.left.accept(self),node.right.accept(self))
            value = np.matmul(node.left.accept(self),node.right.accept(self))
            if node.op =='*=':
                self.memory.register(node.left.name, value)
            return value
        if node.op == '=':
            value = node.right.accept(self)
            self.memory.register(node.left.name, value)
            return value



    @when(AST.IfLine)
    def visit(self, node):
        if node.condition.accept(self):
            return node.line.accept(self)
        else:
            if node.false_line is not None:
                return node.false_line.accept(self)

    # simplistic while loop interpretation
    @when(AST.WhileLine)
    def visit(self, node):
        self.memory.push(Memory("while"))
        r = None
        while node.condition.accept(self):
            try:
                r = node.line.accept(self)
            except BreakException:
                break
            except ContinueException:
                pass

        self.memory.pop()
        return r

    @when(AST.ForLine)
    def visit(self,node):
        self.memory.push(Memory("for"))
        start_index = node.start_index.accept(self)
        end_index = node.end_index.accept(self)
        for index in range(start_index[0][0],end_index[0][0]+1):
            self.memory.register(node.variable.name,[[index]])
            try:
                value = node.line.accept(self)
            except BreakException:
                break
            except ContinueException:
                pass
        self.memory.pop()
        return value

    @when(AST.Lines)
    def visit(self,node):
        for line in node.lines:
            line.accept(self)


    @when(AST.Line)
    def visit(self, node):
        return node.line.accept(self)

    @when(AST.Break)
    def visit(self, node):
        raise BreakException

    @when(AST.Return)
    def visit(self, node):
        raise ReturnException

    @when(AST.Continue)
    def visit(self, node):
        raise ContinueException

    @when(AST.Zeros)
    def visit(self, node):
        width = node.init_expr.accept(self)
        return np.zeros(width,width)

    @when(AST.MatrixLines)
    def visit(self, node):
        return [matrix_line.accept(self) for matrix_line in node.matrix_lines]

    @when(AST.MatrixLine)
    def visit(self, node):
        return [expr.accept(self) for expr in node.expressions]

    @when(AST.MatrixInit)
    def visit(self, node):
        return node.matrix_lines.accept(self)

    @when(AST.Ones)
    def visit(self, node):
        width = node.init_expr.accept(self)
        return np.ones(width, width)

    @when(AST.Eye)
    def visit(self, node):
        width = node.init_expr.accept(self)
        return np.eye(width, width)

    @when(AST.PrintLine)
    def visit(self, node):
        for value in node.values:
            print(value.accept(self))
        return "ok"


