import sys

import ply.lex as lex
import ply.yacc as yacc

import AST
from Interpreter import Interpreter
from TreePrinter import printTree
from TypeChecker import TypeChecker

input = ''
errors = 0
t_ignore = '  \t'

tokens = ['STRING', 'LESSOREQUAL', 'MOREOREQUAL', 'EQUAL', 'NOTEQUAL', 'SUBASSIGN', 'MULASSIGN', 'ADDASSIGN',
          'DIVASSIGN',
          'DOTSUB', 'DOTADD', 'DOTMUL', 'DOTDIV', 'INTNUM', 'FLOATNUM', 'ID', 'TRANSPOSE_SYMBOL']
reserved = {
    'if': 'IF',
    'for': 'FOR',
    'else': 'ELSE',
    'while': 'WHILE',
    'break': 'BREAK',
    'continue': 'CONTINUE',
    'return': 'RETURN',
    'eye': 'EYE',
    'zeros': 'ZEROS',
    'ones': 'ONES',
    'print': 'PRINT'

}
tokens += reserved.values()
# print(tokens)
t_STRING = r'\".*\"'
literals = ['+', '-', '*', '=', '<', '>', '/', '(', ')', '[', ']', '{', '}', '\'', ':', ',', ';']
t_LESSOREQUAL = r'<='
t_MOREOREQUAL = r'>='
t_EQUAL = r'=='
t_NOTEQUAL = r'!='
t_SUBASSIGN = r'-='
t_ADDASSIGN = r'\+='
t_MULASSIGN = r'\*='
t_DIVASSIGN = r'/='
t_DOTSUB = r'\.-'
t_DOTADD = r'\.\+'
t_DOTMUL = r'\.\*'
t_DOTDIV = r'\./'
t_TRANSPOSE_SYMBOL = r'\''

precedence = (

    # ('left', ';'),
    ('right', '=', 'MULASSIGN', 'DIVASSIGN', 'SUBASSIGN', 'ADDASSIGN'),
    ('nonassoc', 'IFX'),
    ('nonassoc', 'ELSE'),
    ('left', ';'),
    ('left', '+', '-', 'DOTADD', 'DOTSUB'),
    ('left', '*', '/', 'DOTMUL', 'DOTDIV'),
    ('left', 'TRANSPOSE_SYMBOL'),
    ('right', 'UMINUS')

)


def t_FLOATNUM(t):
    r'\d+\.\d+'
    t.value = float(t.value)
    t.type = 'FLOATNUM'
    return t


def t_INTNUM(t):
    r'\d+'
    t.value = int(t.value)
    t.type = 'INTNUM'
    return t


def t_ID(t):
    r'[a-zA-Z_]\w*'
    t.type = reserved.get(t.value, 'ID')
    return t


def t_COMMENT(t):
    r'\#.*\n'
    t.lexer.lineno += 1
    pass


def t_newline(t):
    r'(\r\n)+'
    t.lexer.lineno += len(t.value)


def t_newline2(t):
    r'\r+'
    t.lexer.lineno += len(t.value)


def t_newline3(t):
    r'\n+'
    t.lexer.lineno += len(t.value)


def find_column(input, token):
    line_start = input.rfind('\n', 0, token.lexpos) + 1
    return (token.lexpos - line_start) + 1


def t_error(t):
    global input
    global errors
    print("(%d, %d): Illegal character '%s'" % (t.lineno, find_column(input, t), t.value[0]))
    errors += 1
    t.lexer.skip(1)


def p_start(p):
    """start : LINES"""
    print("p[1] =", p[1])
    p[0] = AST.Lines(p[1],lineno=p.lineno(1))


def p_lines(p):
    """LINES : LINES LINE
             | LINE"""
    if len(p) == 3:
        p[0] = AST.Lines(line=p[2], lines=p[1],lineno=p.lineno(1))
    else:
        p[0] = AST.Lines(p[1],lineno=p.lineno(1))


def p_line3(p):
    """LINE : ID '=' EXPR
            | ID SUBASSIGN EXPR
            | ID MULASSIGN EXPR
            | ID ADDASSIGN EXPR
            | ID DIVASSIGN EXPR"""
    p[0] = AST.BinExpr(left=AST.Variable(p[1], lineno=p.lineno(1)), op=p[2], right=p[3], lineno=p.lineno(1))


def p_line5(p):
    """LINE : ID '[' MATRIX_LINE ']' '=' EXPR"""
    p[0] = AST.BinExpr(left=AST.Variable(p[1],tuple(p[3].expressions), lineno=p.lineno(1)), op=p[5], right=p[6], lineno=p.lineno(1))


def p_line2(p):
    """LINE : '{' LINES '}'
            | LINE ';'"""
    if p[1] == '{':
        p[0] = AST.Lines(p[2],lineno=p.lineno(1))
    else:
        p[0] = AST.Line(p[1],lineno=p.lineno(1))


def p_line1(p):
    """LINE : WHILE_LINE
            | FOR_LINE
            | BREAK_LINE
            | CONTINUE_LINE
            | RETURN_LINE
            | PRINT_LINE
            | IF_LINE"""
    p[0] = p[1]


def p_expr(p):
    """EXPR : '-' EXPR %prec UMINUS
            | EXPR TRANSPOSE_SYMBOL"""
    if p[1] == '-':
        p[0] = AST.UnExpr(op=p[1], expr=p[2],lineno=p.lineno(1))
    else:
        p[0] = AST.UnExpr(op=p[2], expr=p[1],lineno=p.lineno(1))


def p_expr4(p):
    """EXPR : EYE_FUN
           | ZEROS_FUN
           | ONES_FUN"""
    p[0] = p[1]

def p_zeros(p):
    """ZEROS_FUN : ZEROS '(' EXPR ')'"""
    p[0]=AST.Zeros(p[3],p.lineno(1))

def p_ones(p):
    """ONES_FUN : ONES '(' EXPR ')'"""
    p[0] = AST.Ones(p[3],p.lineno(1))

def p_eye(p):
    """EYE_FUN : EYE '(' EXPR ')'"""
    p[0] = AST.Eye(p[3],p.lineno(1))

def p_intexpr(p):
    """EXPR : INTNUM"""
    p[0] = AST.IntNum(p[1],lineno=p.lineno(1))

def p_variable_expr(p):
    """EXPR : ID '[' MATRIX_LINE ']'"""
    p[0]=AST.Variable(p[1],tuple(p[3].expressions))

def p_floatexpr(p):
    """EXPR : FLOATNUM"""
    p[0] = AST.FloatNum(p[1],lineno=p.lineno(1))


def p_idexpr(p):
    """EXPR : ID"""
    p[0] = AST.Variable(p[1], lineno=p.lineno(1))


def p_matrix_initexpr(p):
    """EXPR : MATRIX_INITIALIZER"""
    p[0] = p[1]


def p_expr3(p):
    """EXPR : EXPR DOTSUB EXPR
            | EXPR DOTDIV EXPR
            | EXPR DOTMUL EXPR
            | EXPR DOTADD EXPR
            | EXPR '-' EXPR
            | EXPR '*' EXPR
            | EXPR '/' EXPR
            | EXPR '+' EXPR"""
    p[0] = str(p[1]) + str(p[2]) + str(p[3])
    p[0] = AST.BinExpr(left=p[1], op=p[2], right=p[3],lineno=p.lineno(1))


def p_ifline(p):
    """IF_LINE : IF '(' BOOL_EXPR ')' LINE ELSE LINE %prec ELSE
               | IF '(' BOOL_EXPR ')' LINE ';' ELSE LINE %prec ELSE"""
    p[0] = AST.IfLine(condition=p[3], true_line=p[5], false_line=p[7],lineno=p.lineno(1))


def p_ifline2(p):
    """IF_LINE : IF '(' BOOL_EXPR ')' LINE %prec IFX"""
    p[0] = AST.IfLine(condition=p[3], true_line=p[5],lineno=p.lineno(1))


def p_while_line(p):
    """WHILE_LINE : WHILE '(' BOOL_EXPR ')' LINE"""
    p[0] = AST.WhileLine(condition=p[3], line=p[5],lineno=p.lineno(1))


def p_for(p):
    """FOR_LINE : FOR ID '=' EXPR ':' EXPR LINE"""
    p[0] = AST.ForLine(AST.Variable(p[2]), p[4], p[6], p[7], lineno=p.lineno(1))


def p_BOOL_EXPR(p):
    """BOOL_EXPR : EXPR '<' EXPR
                 | EXPR '>' EXPR
                 | EXPR LESSOREQUAL EXPR
                 | EXPR MOREOREQUAL EXPR
                 | EXPR EQUAL EXPR
                 | EXPR NOTEQUAL EXPR"""
    p[0] = AST.BinExpr(left=p[1], op=p[2], right=p[3],lineno=p.lineno(1))


def p_BREAK(p):
    """BREAK_LINE : BREAK"""
    p[0] = AST.Break(lineno=p.lineno(1))


def p_CONTINUE(p):
    """CONTINUE_LINE : CONTINUE"""
    p[0] = AST.Continue(lineno=p.lineno(1))


def p_return(p):
    """RETURN_LINE : RETURN"""
    p[0] = AST.Return(lineno=p.lineno(1))

def p_return2(p):
    """RETURN_LINE : RETURN EXPR
                   | RETURN STRING"""
    p[0] = AST.Return(lineno=p.lineno(1), value=p[2])


def p_print(p):
    """PRINT_LINE : PRINT EXPR
                  | PRINT STRING"""
    p[0] = AST.PrintLine(p[2],lineno=p.lineno(1))

def p_print2(p):
    """PRINT_LINE : PRINT_LINE ',' EXPR
                  | PRINT_LINE ',' STRING"""
    p[0] = AST.PrintLine(value=p[3], values=p[1].values,lineno=p.lineno(1))

def p_print3(p):
    """PRINT_LINE : PRINT_LINE ';'"""
    p[0]=p[1]

def p_matrix_initializer(p):
    """MATRIX_INITIALIZER : '[' MATRIX_LINES ']'"""
    p[0] = AST.MatrixInit(p[2],lineno=p.lineno(1))


def p_matrix_lines(p):
    """MATRIX_LINES : MATRIX_LINE
                    | MATRIX_LINES ';' MATRIX_LINE"""
    try:
        p[0] = AST.MatrixLines(p[3], p[1].matrix_lines,lineno=p.lineno(1))
    except IndexError:
        p[0] = AST.MatrixLines(p[1],lineno=p.lineno(1))


def p_matrix_line(p):
    """MATRIX_LINE : EXPR
                   | MATRIX_LINE ',' EXPR"""
    try:
        p[0] = AST.MatrixLine(expr=p[3], expressions=p[1].expressions,lineno=p.lineno(1))
    except IndexError:
        p[0] = AST.MatrixLine(expr=p[1],lineno=p.lineno(1))


def p_error(p):
    if p is not None:
        print("syntax error in line %d" % p.lineno)
    else:
        print("p is None")


lexer = lex.lex()
parser = yacc.yacc()
with open(sys.argv[1], "r") as fh:
    input = fh.read()
    # lexer.input(input)
    # output = ''
    # for token in lexer:
    #     output += "(%d, %d): %s(%s)\n" % (token.lineno, find_column(input, token), token.type, token.value)
    # if (errors == 0):
    #     print(output)
    # print("for input: " + input)
    # parser.parse(input, lexer=lexer)

if __name__ == '__main__':

    try:
        filename = sys.argv[1] if len(sys.argv) > 1 else "example.txt"
        file = open(filename, "r")
    except IOError:
        print("Cannot open {0} file".format(filename))
        sys.exit(0)

    parser = yacc.yacc()
    text = file.read()
    ast = parser.parse(text, lexer,tracking=True)
    ast.printTree()

    typeChecker = TypeChecker()
    typeChecker.visit(ast)   # or alternatively ast.accept(typeChecker,lineno=p.lineno(1))
    ast.accept(Interpreter())
    # in future
    # ast.accept(OptimizationPass1())
    # ast.accept(OptimizationPass2())
    # ast.accept(CodeGenerator())

